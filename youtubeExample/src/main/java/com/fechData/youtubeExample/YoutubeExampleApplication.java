package com.fechData.youtubeExample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YoutubeExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(YoutubeExampleApplication.class, args);
	}

}
